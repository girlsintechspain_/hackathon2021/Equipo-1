var mapCenter = new google.maps.LatLng(   40.416775,  -3.703790 ); //Google map Coordinates
var map
function initialize() //function initializes Google map
{
var googleMapOptions =
{
center: mapCenter, // map center
zoom: 15, //zoom level, 0 = earth view to higher value
panControl: true, //enable pan Control
zoomControl: true, //enable zoom control
zoomControlOptions: {
  style: google.maps.ZoomControlStyle.SMALL //zoom control size
 },
scaleControl: true, // enable scale control
mapTypeId: google.maps.MapTypeId.ROADMAP // google map type
  };
map = new google.maps.Map(document.getElementById("googleMap"), googleMapOptions);
}






function addMyMarker() { //function that will add markers on button click


  //marker 5
  const contentString5 =
      '<div id="content">' +
          '<div id="siteNotice">' +
          "</div>" +
          '<h2>Tu nuevo evento se ha creado revisa tu correo/h2>' +
          '<div class="date">' +
              "<p>30/10/2021 16:00</p>" +
          "</div>" +
          '<div class="descripcion">' +
              "<p>Esto es una descripción del evento</p>"+
          "</div>" +
      "</div>";

  const infowindow5 = new google.maps.InfoWindow({
      content: contentString5,
  });



var marker = new google.maps.Marker({
  position:mapCenter,
  map: map,
    draggable:true,
    animation: google.maps.Animation.DROP,
  title:"This a new marker!",
        icon: "http://maps.google.com/mapfiles/ms/micons/blue.png"
});


marker.addListener("click", () => {
    infowindow5.open({
      anchor: marker,
      map,
      shouldFocus: false,
    });
});


}
