# Hacking for Humanity Oct 2021

Este es el resultado del trabajo hecho durante la Hackaton2021 de Girls in Tech Spain por el Equipo-1 por Andreu Vinyoles, Isabel Domenech, Gabriela Amadori y Solange Pariona, con la temática **Aplicaciones y webs para mejorar la calidad de vida a las personas con cáncer de mama**.


# Para ver la página subida a producción, porfavor visitad:
https://elquecuidaama.myrealjourney.es/html/index.html


## Solución al reto

Tendrás acceso a la pagina donde podras hablar con especialistas que te aconsejen, o unirte a un grupo en el que hablar con otras personas en tu situación, acompañado también por especialistas y gente que ya haya superado la enfermedad. Por otro lado podrás acceder a un mapa donde encontrarás diversas reuniones presenciales en las que te encontrarás con personas en tu situacion y personal voluntario de apoyo


## Respuesta al reto y beneficios para los usuarios

Las mujeres se sentirán más acompañadas ya que tendrán unos grupos de apoyo tanto de manera telemática como si lo prefieren de forma presencial dependiendo del momento que están viviendo y sus necesidades. Por otro lado tendrán el apoyo de especialistas en la página siempre que lo necesiten.


## Desarollo técnico

Hemos desarrollado una página web con varias funcionalidades.

Por un lado tenemos una parte en la que localizar especialistas tales como nutricionistas, psicólogos fisioterapeutas..que nos puedan acompañar en nuestro proceso para enfrentarnos en mejor estado físico a la enfermedad. Por otro lado tenemos toda una serie de foros donde hablar de lo que nos ocurre acompañados por profesionales voluntarios que nos pueden acompañar y ayudar. A la vez que unas reuniones presenciales a las que nos podemos unir donde también habrá este acompañamiento por parte de personal voluntario y donde podremos hablar con mas gente como nosotros. También habrá un apartado de venta de merchandising para recaudar fondos. Para todo esto implementaremos una página web en la que habrá diversas secciones una de las cuales será un mapa para encontrar los diferentes puntos de reuniones, otra una tienda on line y otra para contactar con especialistas En una segunda fase implementaremos una app donde puedas localizar todas las reuniones que hay en la zona.

La aplicación web tiene un formulario de creación de eventos que esta conectado actualmente a una base de datos. Para mas información se puede visitar la siguiente pagina de borrador para ver el listado de eventos creados de prueba: https://elquecuidaama.myrealjourney.es/html/listardatos.php
En una segunda fase se desarrollara una gestión mas avanzada de base de datos para comunicar a todos aquellos usuarios via correo.

## Autoras
* Andreu Vinyoles [@andreuvinyoles](https://www.linkedin.com/in/andreuvinyoles/)
* Isabel Domenech [@isabel-domenech-de-mena](https://www.linkedin.com/in/isabel-domenech-de-mena-157103124)
* Gabriela Amadori [@gabriela-amadori](https://www.linkedin.com/in/gabriela-amadori/)
* Solange Pariona [@solange-p](https://www.linkedin.com/in/solange-p-42503382/)

## Licencia

Este proyecto utiliza licencia MIT.
